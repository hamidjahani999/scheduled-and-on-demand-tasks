# Scheduled and On Demand Jobs with Run Pipeline Form

## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/cfg-data/write-ci-cd-variables-in-pipeline)

## Demonstrates These Design Requirements, Desirements and AntiPatterns

TL;DR? => [Scheduled and On Demand Tasks Video Walkthrough Demo](https://youtu.be/iGZ-xY1f-B0)

* **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Within a repositories single .gitlab-ci.yml file support both regular pipelines and scheduled and on demand task kick off.
* **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Leverages YAML anchors support to prevent regular jobs from running.
* **GitLab CI Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Leverages variable descriptions to provide forms prompting on the "Run Pipeline" page to prompt for the task name and list the available ones.  [Code is here](https://gitlab.com/guided-explorations/gitlab-ci-yml-tips-tricks-and-hacks/scheduled-and-on-demand-tasks/-/blob/master/.gitlab-ci.yml#L4-6)

### Uses and features for this example:
- purposely avoided using conventional implementation that leverages CI_PIPELINE_SOURCE. One custom variable (TARGETTASKNAME) controls everything. This keeps the implementation simple and expands flexbility in these ways:
  - scheduled tasks can also be run on demand (due to not using CI_PIPELINE_SOURCE)
  - supports multiple GitLab CI jobs being triggered by one “user named **task**” (TARGETTASKNAME)
    - including parallel jobs (example included in the code)
    - including sequenced jobs by defining stages specific to tasks (example included in the code)
    - including multiple independent jobs that simply share the same task name (TARGETTASKNAME) for the convenience of scheduling them all at the same time. (example included in the code)
  - implementation works through the trigger API or any mechanism by which you can kick off GitLab CI that allows a CI variable to be provided
  - due to using just one variable to both indicate a task name and that a task is being used (TARGETTASKNAME), it is easier to deselect the regular, non-task jobs of the pipeline for build, test, deploy, etc.
- uses a yaml anchor to define the code to exclude non-tasks, with a self-documenting name to easily configure and self-document all non-task jobs 
- implements `rules: if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'` to prevent duplicate task runs under specific conditions 

### Cross References and Documentation
* Generic Packages Feature Help: https://docs.gitlab.com/ee/user/packages/generic_packages/
* API to write to project variables: https://docs.gitlab.com/ee/api/project_level_variables.html
* API to write group variables: https://docs.gitlab.com/ee/api/group_level_variables.html

## Guided Explorations Concept

This Guided Exploration is built according to a specific vision and requirements that maximize its value to both GitLab and GitLab's customers.  You can read more here: [The Guided Explorations Concept](https://gitlab.com/guided-explorations/guided-exploration-concept/blob/master/README.md)

## Working Design Pattern

As originally built, this design pattern works and can be tested. In the case of plugin extensions like this one, the working pattern may be it's use in another Guided Exploration.

## Overview Information

GitLab's features are constantly and rapidly evolving and we cannot keep every example up to date.  The date and version information are published here so that you can assess if new features mean that the example could be enhanced or does not account for an new capability of GitLab.

* **Product Manager For This Guided Exploration**: Darwin Sanoy (@DarwinJS)

* **Publish Date**: 2020-11-19

* **GitLab Version Released On**: 13.5

* **GitLab Edition Required**: 

  * For overall solution: [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/) 

    [Click to see Features by Edition](https://about.gitlab.com/features/) 

* **Tested On**: 
  * GitLab Docker-Executor Runner using bash:latest container

* **References and Featured In**:
  * [Scheduled and On Demand Tasks Video Walkthrough Demo](https://youtu.be/iGZ-xY1f-B0)
